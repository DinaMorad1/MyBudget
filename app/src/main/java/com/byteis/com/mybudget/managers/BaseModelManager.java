package com.byteis.com.mybudget.managers;

import com.byteis.com.mybudget.model.Category;
import io.realm.Realm;

/**
 * Created by Dina on 31/08/2016.
 */

public class BaseModelManager {

    protected Realm db;

    //ToDo: key needs to be fixed
    protected int getNextKey() {
        int key;
        try {
            key = db.where(Category.class).max("id").intValue() + 1;
        } catch (Exception ex) {
            key = 0;
        }
        return key;
    }
}
