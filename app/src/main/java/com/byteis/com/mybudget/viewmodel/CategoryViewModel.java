package com.byteis.com.mybudget.viewmodel;

import android.databinding.BaseObservable;
import android.databinding.Bindable;
import com.byteis.com.mybudget.BR;
import com.byteis.com.mybudget.model.Category;

/**
 * Created by Dina on 31/08/2016.
 */

public class CategoryViewModel extends BaseObservable {

    private final Category category;

    public CategoryViewModel(Category category) {
        this.category = category;
    }

    //ToDo: return the database record
    @Bindable
    public String getName() {
//        if (category.isValid()) {
        return category.getName();
//        }
//        return "";
    }

    public void setName(String name) {
        category.setName(name);
        notifyPropertyChanged(BR.name);
    }

}
