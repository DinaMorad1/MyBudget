package com.byteis.com.mybudget.managers;

import android.content.Context;

import com.byteis.com.mybudget.model.Category;

import java.util.ArrayList;

import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by Dina on 31/08/2016.
 */

public class CategoryManager extends BaseModelManager {

    private static CategoryManager _instance;

    private enum DefaultCategory {Food, Entertainment, Shopping, Transportation, Gift, Beauty, Culture}

    public static CategoryManager getInstance(Context context) {
        if (_instance == null)
            _instance = new CategoryManager(context);
        return _instance;
    }

    private CategoryManager(Context context) {
        db = Realm.getInstance(context);
    }

    public void addCategory(Category category) {
        db.beginTransaction();
        db.copyToRealm(category);
        db.commitTransaction();
    }

    public void addCategories(ArrayList<Category> categoriesList) {
        db.beginTransaction();
        for (Category category : categoriesList) {
            db.copyToRealm(category);
        }
        db.commitTransaction();
    }

    public void addDefaultCategories() {
        db.beginTransaction();
        Category category = new Category();
        int index = 1;
        for (DefaultCategory categoryName : DefaultCategory.values()) {
            category.setId(getNextKey());
            category.setName(categoryName.name());
            category.setNum(index);
            db.copyToRealm(category);
            index++;
        }
        db.commitTransaction();
    }

    public Category saveCategory(String name) {
        db.beginTransaction();
        Category category = new Category(name);
        category.setNum(db.where(Category.class).max("num").intValue() + 1);
        category.setId(getNextKey());
        db.copyToRealm(category);
        db.commitTransaction();
        return category;
    }

    public ArrayList<Category> getAllCategories() {
        ArrayList<Category> categoriesArrayList = new ArrayList<>();
        RealmResults<Category> results = db.where(Category.class).findAllSorted("num");
        db.beginTransaction();
        for (int i = 0; i < results.size(); i++) {
            categoriesArrayList.add(results.get(i));
        }
        db.commitTransaction();
        return categoriesArrayList;
    }

    public void updateCategoryNum(int categoryId, int categoryNum) {
        db.beginTransaction();
        Category category = findById(categoryId);
        category.setNum(categoryNum);
        db.copyToRealmOrUpdate(category);
        db.commitTransaction();
    }

    public Category findById(int id) {
        return db.where(Category.class).equalTo("id", id).findFirst();
    }

    public void deleteCategory(final int id) {
        db.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                RealmResults<Category> result = realm.where(Category.class).equalTo("id", id).findAll();
                result.clear();
            }
        });
    }
}