package com.byteis.com.mybudget.viewmodel;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

import com.byteis.com.mybudget.BR;
import com.byteis.com.mybudget.model.Income;

import java.util.Date;

/**
 * Created by Dina on 31/08/2016.
 */

public class IncomeViewModel extends BaseObservable {
    private final Income income;

    public IncomeViewModel(Income income) {
        this.income = income;
    }

    @Bindable
    public double getAmount() {
//        if (category.isValid()) {
        return income.getAmount();
//        }
//        return "";
    }

    public void setAmount(double amount) {
        income.setAmount(amount);
        notifyPropertyChanged(BR.amount);
    }

//    @Bindable
//    public int geType() {
////        if (category.isValid()) {
//        return income.getType();
////        }
////        return "";
//    }
//
//    public void setType(int type) {
//        income.setType(type);
//        notifyPropertyChanged(BR.type);
//    }

//    @Bindable
//    public Date geDate() {
//        if (income.isValid()) {
//        return income.getDate();
//        }
//        return new Date();
//    }
//
//    public void setDate(Date date) {
//        income.setDate(date);
//        notifyPropertyChanged(BR.date);
//    }

}
