package com.byteis.com.mybudget.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.byteis.com.mybudget.R;
import com.byteis.com.mybudget.databinding.CategoryCellBinding;
import com.byteis.com.mybudget.listeners.ItemTouchHelperAdapter;
import com.byteis.com.mybudget.managers.CategoryManager;
import com.byteis.com.mybudget.model.Category;
import com.byteis.com.mybudget.viewmodel.CategoryViewModel;

import java.util.Collections;
import java.util.List;

/**
 * Created by Dina on 30/08/2016.
 */

public class CategoriesAdapter extends BaseAdapter implements ItemTouchHelperAdapter {

    private CategoryManager categoryManager;
    private Context context;

    public CategoriesAdapter(List items, Context context) {
        super(items);
        this.categoryManager = CategoryManager.getInstance(context);
        this.context = context;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public final CategoryCellBinding binding;

        ViewHolder(final CategoryCellBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (layoutInflater == null)
            layoutInflater = LayoutInflater.from(parent.getContext());

        final CategoryCellBinding binding =
                DataBindingUtil.inflate(layoutInflater, R.layout.category_cell, parent, false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        super.onBindViewHolder(viewHolder, position);
        int type = getItemViewType(position);

        if (type == TYPE_EMPTY)
            return;

        final ViewHolder holder = (ViewHolder) viewHolder;
        Category category = (Category) items.get(position);
        CategoryViewModel categoryViewModel = new CategoryViewModel(category);
        holder.binding.setCategory(categoryViewModel);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public void onItemDismiss(int position) {
        CategoryManager.getInstance(context).deleteCategory(((Category)items.get(position)).getId());
        items.remove(position);
        notifyItemRemoved(position);
    }

    @Override
    public boolean onItemMove(int fromPosition, int toPosition) {
        if (fromPosition < toPosition) {
            for (int i = fromPosition; i < toPosition; i++) {
                Collections.swap(items, i, i + 1);
            }

            categoryManager.updateCategoryNum(((Category) items.get(fromPosition)).getId(), ((Category) items.get(toPosition)).getNum());




        } else {
            for (int i = fromPosition; i > toPosition; i--) {
                Collections.swap(items, i, i - 1);
            }

            categoryManager.updateCategoryNum(((Category) items.get(fromPosition)).getId(), ((Category) items.get(toPosition)).getNum());


        }
        notifyItemMoved(fromPosition, toPosition);
        return true;
    }

}
