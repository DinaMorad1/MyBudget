package com.byteis.com.mybudget.ui;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.byteis.com.mybudget.R;
import com.byteis.com.mybudget.databinding.ActivityBudgetBinding;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class BudgetActivity extends BaseActivity implements DatePickerDialog.OnDateSetListener {

    private ActivityBudgetBinding binding;
    private DatePickerDialog datePickerDialog;
    public static final String DATEPICKER_TAG = "datepicker";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.activity_budget, null, false);
        inflateContent(binding.getRoot());

        initDatePicker();
        initSpinner();
    }

    private void initSpinner() {
        List<CharSequence> items = new ArrayList<CharSequence>();
        items.add("item1");
        items.add("item2");
        ArrayAdapter<CharSequence> adapter = new ArrayAdapter<CharSequence>(this, R.layout.spinner_item, items);
        binding.spinner.setAdapter(adapter);

    }

    private void initDatePicker() {
        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        datePickerDialog = DatePickerDialog.newInstance(this, year, month, day);
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {

    }

    public void onSelectDateButtonClicked(View view) {
        datePickerDialog.show(getFragmentManager(), DATEPICKER_TAG);
    }
}
