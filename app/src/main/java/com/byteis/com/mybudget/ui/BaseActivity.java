package com.byteis.com.mybudget.ui;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.RelativeLayout;

import com.byteis.com.mybudget.R;
import com.byteis.com.mybudget.databinding.ActivityBaseBinding;

/**
 * Created by Dina on 30/08/2016.
 */

public class BaseActivity extends AppCompatActivity {

    private ActivityBaseBinding baseBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        baseBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.activity_base, null, false);
        setContentView(baseBinding.getRoot());

        initToolbar();
    }

    private void initToolbar() {
        if (baseBinding.toolbar != null) {
            setSupportActionBar((baseBinding.toolbar).toolbar);
        }
    }

    protected void inflateContent(View view) {
        RelativeLayout contentContainerLayout = baseBinding.baseCustomView;
        view.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT));
        contentContainerLayout.removeAllViews();
        contentContainerLayout.addView(view);
    }

    protected void initRecyclerView(RecyclerView recyclerView, RecyclerView.Adapter adapter) {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(adapter);
        recyclerView.setItemAnimator(null);
    }

    protected void hideKeyboard() {
        InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
    }


}
