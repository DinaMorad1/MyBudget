package com.byteis.com.mybudget.ui;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.View;

import com.byteis.com.mybudget.R;
import com.byteis.com.mybudget.adapter.CategoriesAdapter;
import com.byteis.com.mybudget.callbacks.SimpleItemTouchHelperCallback;
import com.byteis.com.mybudget.databinding.ActivityCategoriesBinding;
import com.byteis.com.mybudget.managers.CategoryManager;
import com.byteis.com.mybudget.model.Category;

import java.util.ArrayList;

/**
 * Created by Dina on 30/08/2016.
 */

public class CategoriesActivity extends BaseActivity {

    private ActivityCategoriesBinding binding;
    private CategoriesAdapter categoriesAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.activity_categories, null, false);
        inflateContent(binding.getRoot());
        initItems();
    }

    private void initItems() {
        ArrayList<Category> categoriesArrayList = CategoryManager.getInstance(this).getAllCategories();
        categoriesAdapter = new CategoriesAdapter(categoriesArrayList, this);
        initRecyclerView();
    }

    public void onAddCategoryClicked(View view) {
        Category addedCategory = CategoryManager.getInstance(this).saveCategory(binding.categoryNameEditText.getText().toString());
        binding.categoryNameEditText.setText("");
        hideKeyboard();
        categoriesAdapter.addItem(addedCategory, categoriesAdapter.getItemCount());

    }

    protected void initRecyclerView() {
        RecyclerView recyclerView = binding.categoriesRecyclerView;
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(categoriesAdapter);
        recyclerView.setItemAnimator(null);

        /*Drag and drop to change item position*/
        ItemTouchHelper.Callback callback =
                new SimpleItemTouchHelperCallback(categoriesAdapter);
        ItemTouchHelper touchHelper = new ItemTouchHelper(callback);
        touchHelper.attachToRecyclerView(recyclerView);
    }
}
