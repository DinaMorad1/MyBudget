package com.byteis.com.mybudget.data;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Created by Dina on 31/08/2016.
 */

public class SharedPref {

    public static final String IS_FIRST_APP_LAUNCH = "IS_FIRST_APP_LAUNCH";

    /*
  * Check for the value's reference{@param key} existence in android standard preference
  * file.
  *
  * @param key: The reference for the value that supposed to be stored at preferences
  * file.
  * @param context: The application context that represent the current state
  * of the application.
  * */
    public static boolean checkKey(Context context, String key) {
        return PreferenceManager.getDefaultSharedPreferences(context).contains(key);
    }


    /*
    * Load a saved integer value from preferences file by its associated
    * reference{@ param key}.
    *
    * @param key: The reference for the value that supposed to be stored at preferences
    * file.
    * @param context: The application context that represent the current state
    * of the application.
    *
    * @return: Returns the associated int value with the {@param key} or "0" if the key was
    * not presented.
    * */
    public static int LoadInt(Context context, String key) {
        /*
        * prefs: A preference instance. It should be loaded each time according to
        * application state.
        * */
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
       /* int data;

        if(prefs.contains(key)){
            data = prefs.getInt(key,0);
        }*//*else{
            data = 0;
        }*/
        return prefs.getInt(key, 0);
    }

    /*
    * Save an integer value at preferences file and associate a reference {@param key}
    * for it.
    *
    * @param context: The application context that represent the current state
    * of the application.
    * @param key: The reference for the value that will be stored.
    * @param value: The int value to be stored.
    * */
    public static void SaveInt(Context context, String key, int value) {
        /*
        * prefs: A preference instance. It should be loaded each time according to
        * application state.
        * */
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        /*
        * prefEditor: The preferences standard editor.
        * */
        SharedPreferences.Editor prefEditor = prefs.edit();
        prefEditor.putInt(key, value);
        /*
        * complete saving {@param value}
        * */
        prefEditor.apply();

    }

    /*
    * Load a saved string value from preferences by its associated reference{@ param key}.
    *
    * @param key: The reference for the value that supposed to be stored at preferences
    * file.
    * @param context: The application context that represent the current state
    * of the application.
    *
    * @return: Returns the associated string value with the reference{@ param key} or
    * "" if the key not presented.
    * */
    public static String LoadString(Context context, String key) {
        /*
        * prefs: A preference instance. It should be loaded each time according to
        * application state.
        * */
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
       /* String data;

        if(prefs.contains(key)){
            data = prefs.getString(key,"");
        }else{
            data = "";
        }*/
        return prefs.getString(key, "");
    }

    /*
    * Save a string value at preferences file and associate a reference{@param key}
    * for it.
    *
    * @param context: The application context that represent the current state
    * of the application.
    * @param key: The reference for the string value that will be stored.
    * @param value: The string value to be stored.
    * */
    public static void SaveString(Context context, String key, String value) {
        /*
        * prefs: A preference instance. It should be loaded each time according to
        * application state.
        * */
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        /*
        * prefEditor: The preferences standard editor.
        * */
        SharedPreferences.Editor prefEditor = prefs.edit();
        prefEditor.putString(key, value);
        /*
        * complete saving {@param value}
        * */
        prefEditor.apply();
    }

    /*
    * Load a saved boolean value from preferences by its associated reference{@ param key}.
    *
    * @param key: The key for the value that supposed to be stored at preferences file.
    * @param context: The application context that represent the current state
    * of the application.
    *
    * @return: Returns the associated boolean value with the reference{@ param key} or
    * "false" if the key not presented.
    * */
    public static boolean LoadBoolean(Context context, String key) {
        /*
        * prefs: A preference instance. It should be loaded each time according to
        * application state.
        * */
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        /*boolean data;

        if(prefs.contains(key)){
            data = prefs.getBoolean(key,false);
        }else{
            data = false;
        }*/
        return prefs.getBoolean(key, false);
    }

    /*
    * Save a boolean value at preferences file and associate a reference{@param key}
    * for it.
    *
    * @param context: The application context that represent the current state
    * of the application.
    * @param key: The reference for the boolean value that will be stored.
    * @param value: The boolean value to be stored.
    * */
    public static void SaveBoolean(Context context, String key, boolean value) {
        /*
        * prefs: A preference instance. It should be loaded each time according to
        * application state.
        * */
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        /*
        * prefEditor: The preferences standard editor.
        * */
        SharedPreferences.Editor prefEditor = prefs.edit();
        prefEditor.putBoolean(key, value);
        /*
        * complete saving {@param value}
        * */
        prefEditor.apply();
    }

    /*
    * Load a saved long value from preferences file by its associated reference{@ param key}.
    *
    * @param key: The key(reference) for the value that supposed to be stored at preferences
    * file.
    * @param context: The application context that represent the current state
    * of the application.
    *
    * @return: Returns the associated long value with the reference{@ param key} or
    * "0" if the key not presented.
    * */
    public static long LoadLong(Context context, String key) {
        /*
        * prefs: A preference instance. It should be loaded each time according to
        * application state.
        * */
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        return prefs.getLong(key, 0);
    }

    /*
    * Save a long value at preferences file and associate a reference {@param key}
    * for it.
    *
    * @param context: The application context that represent the current state
    * of the application.
    * @param key: The reference for the long value that will be stored.
    * @param value: The long value to be stored.
    * */
    public static void SaveLong(Context context, String key, long value) {
        /*
        * prefs: A preference instance. It should be loaded each time according to
        * application state.
        * */
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        /*
        * prefEditor: The preferences standard editor.
        * */
        SharedPreferences.Editor prefEditor = prefs.edit();
        prefEditor.putLong(key, value);
        /*
        * complete saving {@param value}
        * */
        prefEditor.apply();
    }

    /*
    * Delete all values stored at preference file.
    *
    * @param context: The application context that represent the current state
    * of the application.
    * */
    public static void deleteShared(Context context) {
           /*
        * prefs: A preference instance. It should be loaded each time according to
        * application state.
        * */
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        /*
        * prefEditor: The preferences standard editor.
        * */
        SharedPreferences.Editor prefEditor = prefs.edit();
        /*
        * clear all preferences saved values.
        * */
        prefEditor.clear();
        /*
        * complete the delete operation.
        * */
        prefEditor.apply();
    }
}
