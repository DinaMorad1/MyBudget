package com.byteis.com.mybudget.model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Dina on 30/08/2016.
 */

public class Category extends RealmObject {

    public Category() {
    }

    public Category(String name) {
        this.name = name;
    }

    @PrimaryKey
    private int id;

    private String name;

    private String note;

    private int num;

    /*getters and setters*/
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
