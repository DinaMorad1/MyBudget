package com.byteis.com.mybudget.data;

import android.app.Application;

import com.byteis.com.mybudget.managers.CategoryManager;

/**
 * Created by Dina on 31/08/2016.
 */

public class MyBudgetApp extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        initDefaultCategories();
    }

    private void initDefaultCategories() {
        if (!SharedPref.checkKey(this, SharedPref.IS_FIRST_APP_LAUNCH)) {
            CategoryManager.getInstance(this).addDefaultCategories();
            SharedPref.SaveBoolean(this, SharedPref.IS_FIRST_APP_LAUNCH, false);
        }
    }
}
