package com.byteis.com.mybudget.listeners;

/**
 * Created by Dina on 31/08/2016.
 */

public interface ItemTouchHelperAdapter {
    boolean onItemMove(int fromPosition, int toPosition);

    void onItemDismiss(int position);
}
